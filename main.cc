#include <nan.h>
#include <opencv2/opencv.hpp>
#include <iostream>
#include "FaceDetector.hpp"


using namespace dlib;
using namespace std;
using namespace cv;

using v8::FunctionTemplate;
using v8::Handle;
using v8::Object;
using v8::String;
using Nan::GetFunction;
using Nan::New;
using Nan::Set;




std::vector<Mat> loadedImage;


void Method(const Nan::FunctionCallbackInfo<v8::Value>& info){
  //info.GetReturnValue().Set(Nan::New("world").ToLocalChecked());
  if (info.Length() != 1){
    Nan::ThrowTypeError("Wrong number of arguments\nONLY INJECT FOLDER PATCH!");
    return;
  }

  FaceDetector detector;

  detector.LoadNet("mmod_human_face_detector.dat");

  string imagesDirectory = *Nan::Utf8String(info[0]);

  cout << "Start load all images in : " << imagesDirectory << "..." << endl;

  std::vector<cv::String> fn; //must be cv::String. careful
  glob(imagesDirectory, fn, false);

  loadedImage.clear();

    for(size_t i = 0; i < fn.size(); i++){
      cout << fn[i] << endl;
      loadedImage.push_back(imread(fn[i]));

      matrix<rgb_pixel> matrix = detector.MatToMatrix(loadedImage[i]);
      auto detectedFaces = detector.detect(matrix);
      for(mmod_rect Facerect : detectedFaces){
          cout << Facerect.rect << endl;
          cv::Point p1(Facerect.rect.left(), Facerect.rect.top());
          cv::Point p2(Facerect.rect.right(), Facerect.rect.bottom());
          FaceDetector::drawFaceRect(loadedImage[i], p1, p2);
		  
		  string imagePath = "init_images/" + to_string(i) + " .jpg";
          imwrite(imagePath, loadedImage[i]);

      }
    }


}

NAN_MODULE_INIT(InitAll) {

  Set(target, New<v8::String>("DetectFacesInFolder").ToLocalChecked(),
    GetFunction(New<FunctionTemplate>(Method)).ToLocalChecked());
}

NODE_MODULE(openk, InitAll)
