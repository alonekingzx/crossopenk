#include "dlib/dnn.h"
#include "dlib/opencv.h"

using namespace dlib;
using namespace std;
using namespace cv;

class FaceDetector{
private:
  // ----------------------------------------------------------------------------------------

  template <long num_filters, typename SUBNET> using con5d = con<num_filters,5,5,2,2,SUBNET>;
  template <long num_filters, typename SUBNET> using con5  = con<num_filters,5,5,1,1,SUBNET>;

  template <typename SUBNET> using downsampler  = relu<affine<con5d<32, relu<affine<con5d<32, relu<affine<con5d<16,SUBNET>>>>>>>>>;
  template <typename SUBNET> using rcon5  = relu<affine<con5<45,SUBNET>>>;

  using net_type = loss_mmod<con<1,9,9,1,1,rcon5<rcon5<rcon5<downsampler<input_rgb_image_pyramid<pyramid_down<6>>>>>>>>;

  // ----------------------------------------------------------------------------------------
public:
  net_type detector;
  matrix<rgb_pixel> MatToMatrix(Mat image){
    cv_image<bgr_pixel> cvImage(image);
    matrix<rgb_pixel> matrix;
    assign_image(matrix, cvImage);

    return matrix;
  }

  std::vector<mmod_rect> detect(matrix<rgb_pixel> image){
    auto dets = detector(image);
    cout << "detected " << dets.size() << " face(s)" << endl;

    return dets;
  }

  void LoadNet(string modelPath){
      deserialize(modelPath) >> detector;
      cout << "loaded model " << modelPath << endl;
  }

  static void drawFaceRect(Mat& image, cv::Point p1, cv::Point p2){
    cv::rectangle(image, p1, p2, Scalar(0, 255, 0), 3);

  }
};
